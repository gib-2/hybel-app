from django.apps import AppConfig


class HybeldbConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'hybeldb'
