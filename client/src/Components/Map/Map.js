/*import mapboxgl from 'mapbox-gl';
mapboxgl.accessToken = 'pk.eyJ1IjoiaGVsZW5yZWIiLCJhIjoiY2t6ZHdzd3NpMGVnNDMycGQ5dGxzZng4MiJ9.2-fC3a6jg0gEbbXHeMOkaQ';

export function Map(){
    navigator.geolocation.getCurrentPosition(successLocation, 
        errorLocation, {
            enableHighAccuracy : true
    })
    
    function successLocation(position){
        console.log(position)
        setupMap([position.coords.longitude, position.coords.latitude])
    }
    function errorLocation(){
        setupMap([10.392634, 63.429059])
    }
    
    function setupMap(center){
        const map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v11',
            center: center,
            zoom: 15
        })
        var nav = new mapboxgl.NavigationControl();
        map.addControl(nav);    
    }
    return(
        <div ref = {setUpMap}/>
    )

}

export default Map; */
import React, {useState, useEffect, useRef} from 'react'
import ReactMapGL, {NavigationControl} from 'react-map-gl'
import mapboxgl from '!mapbox-gl'; // eslint-disable-line import/no-webpack-loader-syntax
 //<div id = 'map' ref={mapContainer} className="map-container" />
mapboxgl.accessToken = 'pk.eyJ1IjoiaGVsZW5yZWIiLCJhIjoiY2t6ZHdzd3NpMGVnNDMycGQ5dGxzZng4MiJ9.2-fC3a6jg0gEbbXHeMOkaQ';
//import {NavigationControl} from 'react-map-gl'

export function Map(){
    const mapContainer = useRef(null);
    const nav = new mapboxgl.NavigationControl();
    const map = useRef(null);
    const [lng, setLng] = useState(10.392634);
    const [lat, setLat] = useState(63.429059);
    const [zoom, setZoom] = useState(15);

    async function setCenter(){
        console.log('hei')
        navigator.geolocation.getCurrentPosition(successLocation, 
            errorLocation, {
                enableHighAccuracy : true
        })
    }
    function successLocation(position){
        console.log(position)
        setLng([position.coords.longitude])
        setLat([position.coords.latitude])
    }
    function errorLocation(){
    }
    useEffect(() => {
      if (map.current) return; // initialize map only once
        setCenter()
        map.current = new mapboxgl.Map({
            container: mapContainer.current,
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [lng, lat],
            zoom: zoom
        })
    });

    useEffect(() => {
        if (!map.current) return; // wait for map to initialize
        map.current.on('move', () => {
        setLng(map.current.getCenter().lng.toFixed(4));
        setLat(map.current.getCenter().lat.toFixed(4));
        setZoom(map.current.getZoom().toFixed(2));
        });
        });
  
    return (
      <div>
          <div className="sidebar">
            Longitude: {lng} | Latitude: {lat} | Zoom: {zoom}
        </div>
        <div id = 'map' ref={mapContainer} className="map-container" />
      </div>
      
    )
}
export default Map