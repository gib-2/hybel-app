
mapboxgl.accessToken = 'pk.eyJ1IjoiaGVsZW5yZWIiLCJhIjoiY2t6ZHdzd3NpMGVnNDMycGQ5dGxzZng4MiJ9.2-fC3a6jg0gEbbXHeMOkaQ';

navigator.geolocation.getCurrentPosition(successLocation, 
    errorLocation, {
        enableHighAccuracy : true
})

function successLocation(position){
    console.log(position)
    setupMap([position.coords.longitude, position.coords.latitude])
}
function errorLocation(){
    setupMap([10.392634, 63.429059])
}

function setupMap(center){
    const map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v11',
        center: center,
        zoom: 15
    })
    var nav = new mapboxgl.NavigationControl();
    map.addControl(nav);    
}