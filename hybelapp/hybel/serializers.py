
from rest_framework import serializers
from hybelapp.hybel.models import User, Hybel


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['hybel_id', 'adresse', 'kvadratmeter', 'ant_soverom', 'n_koordinat', 'e_koordinat',
                  'leiepris', 'strøm_inkl', 'wifi_inkl', 'start_leie', 'slutt_leie', 'user']


class HybelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Hybel
        fields = ['user_id', 'surname', 'lastname', 'epost', 'tlfnr']
