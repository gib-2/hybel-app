from django.db import models

# Create your models here.


class Hybel(models.Model):
    """A model of a hybel."""
    hybel_id = models.AutoField(primary_key=True)
    adresse = models.CharField(max_length=200)
    kvadratmeter = models.IntegerField()
    ant_soverom = models.IntegerField(max_length=2)
    n_koordinat = models.IntegerField(max_length=7)
    e_koordinat = models.IntegerField(max_length=7)
    leiepris = models.IntegerField()
    strøm_inkl = models.CharField(1)
    wifi_inkl = models.CharField(1)
    start_leie = models.DateTimeField()
    slutt_leie = models.DateTimeField()

    user = models.ForeignKey("User")


class User(models.Model):
    """A model of a user."""
    user_id = models.AutoField(primary_key=True)
    surname = models.CharField(max_length=20)
    lastname = models.CharField(max_length=50)
    epost = models.CharField(max_length=200)
    tlfnr = models.IntegerField(max_length=8)
