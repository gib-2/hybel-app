from django.shortcuts import render

# Create your views here.
from rest_framework import viewsets
from rest_framework import permissions
from hybelapp.hybel.models import Hybel, User
from hybelapp.hybel.serializers import HybelSerializer, UserSerializer


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticated]


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Hybel.objects.all()
    serializer_class = HybelSerializer
    permission_classes = [permissions.IsAuthenticated]
